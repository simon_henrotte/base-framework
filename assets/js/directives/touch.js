var touch = angular.module('touchEvents', []),
    hmGestures = ['touchHold:hold',
        'touchTap:tap',
        'touchDoubletap:doubletap',
        'touchDrag:drag',
        'touchDragstart:dragstart',
        'touchDragend:dragend',
        'touchDragup:dragup',
        'touchDragdown:dragdown',
        'touchDragleft:dragleft',
        'touchDragright:dragright',
        'touchSwipe:swipe',
        'touchSwipeup:swipeup',
        'touchSwipedown:swipedown',
        'touchSwipeleft:swipeleft',
        'touchSwiperight:swiperight',
        'touchTransformstart:transformstart',
        'touchTransform:transform',
        'touchTransformend:transformend',
        'touchRotate:rotate',
        'touchPinch:pinch',
        'touchPinchin:pinchin',
        'touchPinchout:pinchout',
        'touchTouch:touch',
        'touchRelease:release'
    ];

angular.forEach(hmGestures, function(name) {
    var directive = name.split(':'),
        directiveName = directive[0],
        eventName = directive[1];
    touch.directive(directiveName, ["$parse",
        function($parse) {
            return {
                scope: true,
                link: function(scope, element, attr) {
                    var fn, opts;
                    fn = $parse(attr[directiveName]);
                    opts = $parse(attr["hmOptions"])(scope, {});
                    if (opts && opts.group) {
                        scope.hammer = scope.hammer || Hammer(element[0], opts);
                    } else {
                        scope.hammer = Hammer(element[0], opts);
                    }
                    return scope.hammer.on(eventName, function(event) {
                        return scope.$apply(function() {
                            return fn(scope, {
                                $event: event
                            });
                        });
                    });
                }
            };
        }
    ]);
});