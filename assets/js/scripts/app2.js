function setPassedDays() {
    var elPassed = $('.passed');
    for (var i = 0; i < elPassed.length; i++) {
        $(elPassed[i]).append('<span class="past"></span>');
    }
}

function setBarSize(el, minWidth) {
    var bar = el.find('.bar');
    for (var i = 0; i < bar.length; i++) {
        var days = $(bar[i]).attr('data-days');
        var width = (minWidth * days) + 'px';
        $(bar[i]).css({
            'width': width,
            'overflow':'visible'
        });
    }
    return true;
}

function setProgressBar(el) {
    var progress = el.find('.progress');
    for (var i = 0; i < progress.length; i++) {
        var progressing = $(progress[i]).attr('data-progress');
        $(progress[i]).css({
            'width': progressing+'%'
        });
    }
}

function getToday(minWidth) {
    var dayPassed = $('.projects_table thead').attr('data-dayPassed');
    toScroll = dayPassed * minWidth;
    $('.projects_container').animate({
        "scrollLeft": toScroll
    }, "slow");
}

function projectInit() {
    var el = $('.projects_container');
    var minWidth = $('.projects_table td:last-child').css('min-width');
    minWidth = minWidth.replace('px', '');
    getToday(minWidth);
    setPassedDays();
    if (setBarSize(el, minWidth)) {
        setProgressBar(el, minWidth);
    }
}

angular
    .module('projects', ['clicker', 'resizeElem', 'nomad_project'])
    .controller('projects', ['$scope', '$http',
        function($scope, $http) {
            $scope.displayer = false;
            $scope.dataLoaded = false;
            $scope.editProject = true;
            $scope.compileDone = false;
            $scope.assign = false;
            $scope.zoom = 1;
            $scope.display_project = $scope.isolate_projects = $scope.display_worker = $scope.isolate_worker = [];
            $scope.project = {};
            $scope.backup_projects = {};
            $scope.three = {};
            $scope.newAssignement = $scope.infoAssignement = false;

            $http.get('assets/js/data/structure.json').success(function(data) {
                $scope.projects = data.projects;
                $scope.workers = data.workers;
                $scope.dataLoaded = true;
            });

            // Hightlight the project and the worker
            $scope.highlight_project = function(projects, workers) {
                if ($scope.displayer == false) {
                    $scope.display_project = projects;
                    $scope.display_worker = workers;
                    $scope.displayer = true;
                } else {
                    $scope.display_project = $scope.display_worker = [];
                    $scope.displayer = false;
                }
            }

            // Isolate the project and workers
            $scope.isolate_project = function(projects, workers) {
                if ($scope.displayer == false) {
                    $scope.isolate_projects = projects;
                    $scope.isolate_worker = workers;
                    $scope.displayer = true;
                } else {
                    $scope.isolate_projects = $scope.isolate_worker = [];
                    $scope.displayer = false;
                }
            }

            $scope.deploy = function($event, id, level) {
                $event.stopPropagation();
                $scope.three[id] = level;
            }
            $scope.$watch('compileDone', function(newValue, oldValue, scope) {
                if (newValue == true) {
                    projectInit();
                }
            });

            $scope.zoomToday = function() {
                var elem = $('#today');
                var offset = elem.offset().left - elem.parent().offset().left;
                $('.projects_container').animate({
                    scrollLeft: offset
                }, "slow");
            }

            $scope.assignement = function() {
                $scope.assign = !$scope.assign;
            }

            $scope.addAssign = function(e) {
                e.stopPropagation();
                $scope.newAssignement = !$scope.newAssignement;
                if ($scope.infoAssignement) {
                    $scope.infoAssignement = !$scope.infoAssignement;
                }
                console.log(e.clientX);
                console.log(e.clientY);
                $('#newAssignment').css({
                    'left': (e.pageX) - 20 + 'px',
                    'top': ((e.pageY) - ($('#newAssignment').innerHeight() / 2) - 20) + 'px'
                });
            }

            $scope.infoAssign = function(e) {
                e.stopPropagation();
                $scope.infoAssignement = !$scope.infoAssignement;
                if ($scope.newAssignement) {
                    $scope.newAssignement = !$scope.newAssignement;
                }
                $('#infoAssignment').css({
                    'left': (e.pageX) - 10 + 'px',
                    'top': ((e.pageY) - ($('#infoAssignment').innerHeight() / 2) - 20) + 'px'
                });
            }

            $scope.toggleEdit = function() {
                $scope.editProject = !$scope.editProject;
            }

            $scope.zoomLevel = function(zoom) {
                $scope.zoom = zoom;
                console.log($scope.zoom);
            }

            $scope.cancelEdit = function() {
                $scope.toggleEdit();
                angular.copy($scope.backup_projects, $scope.project);
            }

            $scope.saveEdit = function() {
                $scope.toggleEdit();
                $scope.backup_projects = angular.copy($scope.project);
            }
        }
    ]);