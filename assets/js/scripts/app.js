function test(){
    console.log('this is done');
}
angular
    .module('projects', ['clicker'])
    .controller('projects', ['$scope', '$http',
        function($scope, $http) {
            $scope.compileDone = false;
            $scope.displayer = false;
            $scope.dataLoaded = false;
            $scope.display_project = $scope.isolate_projects = $scope.display_worker = $scope.isolate_worker = [];

            $http.get('assets/js/data/structure.json').success(function(data) {
                console.log('data loaded');
                $scope.projects = data.projects;
                $scope.workers = data.workers;
                $scope.dataLoaded = true;
            });

            // Hightlight the project and the worker
            $scope.highlight_project = function(projects, workers) {
                if ($scope.displayer == false) {
                    $scope.display_project = projects;
                    $scope.display_worker = workers;
                    $scope.displayer = true;
                } else {
                    $scope.display_project = $scope.display_worker = [];
                    $scope.displayer = false;
                }
            }
            $scope.isolate_project = function(projects, workers) {
                if ($scope.displayer == false) {
                    $scope.isolate_projects = projects;
                    $scope.isolate_worker = workers;
                    $scope.displayer = true;
                } else {
                    $scope.isolate_projects = $scope.isolate_worker = [];
                    $scope.displayer = false;
                }
            }
            $scope.$watch('compileDone',function (newValue,oldValue,scope){
                console.log($scope.compileDone);
                if(newValue == true){
                    test();
                }
            });
        }
    ]);