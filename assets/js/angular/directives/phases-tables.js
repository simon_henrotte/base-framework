var buildTablePhaseDirective;
var mouseX;
var mouseY;

function parseDate(date) {
    var mdy = date.split('-');
    return new Date(mdy[0], mdy[1] - 1, mdy[2]);
}
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function findEarliestDate(dates) {
    if (dates.length == 0) return null;
    var earliestDate = new Date(dates[0]);
    for (var i = 0; i < dates.length; i++) {
        var currentDate = new Date(dates[i]);
        if (currentDate < earliestDate) {
            earliestDate = currentDate;
        }
    }
    return earliestDate;
}

function findLastedDate(dates) {
    if (dates.length == 0) return null;
    var latestDate = new Date(dates[0]);
    for (var i = 0; i < dates.length; i++) {
        var currentDate = new Date(dates[i]);
        if (currentDate > latestDate) {
            latestDate = currentDate;
        }
    }
    return latestDate;
}

function getNumberDays(firstDate, lastDate) {
    return numberDays = Math.floor((lastDate - firstDate) / (1000 * 60 * 60 * 24));
}


function getTableDates(start_dates, end_dates, isHead, className, firstDateAll, lastDateAll, progress, zoomAttr, isLeaf) {
    var today = new Date();
    today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    var first = new Date(findEarliestDate(start_dates));
    var last = new Date(findLastedDate(end_dates));
    var stockYear = first.getFullYear();
    if (zoomAttr === null || zoomAttr === undefined) {
        zoomAttr = 'days';
    }
    if (isLeaf === null || isLeaf === undefined) {
        isLeaf = false;
    }
    if (zoomAttr == 'days') {
        if (isHead) {
            var line = "";
            var week_line = "";

            // All days lines
            for (var date = new Date(firstDateAll); date <= lastDateAll; date.setDate(date.getDate() + 1)) {
                //if(stockYear == date.getFullYear()){       
                if (today.getTime() === date.getTime()) {
                    line += '<td class="today" id="today">' + date.getDate() + '<span class="month">/' + (date.getMonth() + 1) + '</span></td>';
                } else if (date.getTime() < today.getTime()) {
                    line += '<td class="passed">' + date.getDate() + '<span class="month">/' + (date.getMonth() + 1) + '</span></td>';
                } else {
                    line += '<td>' + date.getDate() + '<span class="month">/' + (date.getMonth() + 1) + '</span></td>';
                }

                // }else{
                //     stockYear = date.getFullYear();
                //     line += '<td><span class="year">' + stockYear + '</span></td>';
                //     line += '<td>'+ date.getDate() +'<span class="month">/'+ (date.getMonth()+1) +'</span></td>';
                // }
            }

            // Week days lines
            // Month days lines
            return line;
        } else {
            var line = "";
            var numberDays = getNumberDays(new Date(first), new Date(last)) + 1;
            for (var date = new Date(firstDateAll); date <= lastDateAll; date.setDate(date.getDate() + 1)) {
                if (date.getDate() == first.getDate() && (date.getMonth() + 1) == (first.getMonth() + 1) && date.getFullYear() == first.getFullYear()) {
                    if (date.getTime() < today.getTime()) {
                        if (isLeaf) {
                            line += '<td class="passed">';
                                line += '<div class="bar bar--' + className + '" data-days="' + numberDays + '" canresize="" ng-click="infoAssign($event);">';
                                    line += '<div class="progress leaf" data-progress="' + progress + '">';
                                    line += '</div>';
                                    line +='<span class="progress-text">' + progress + '%</span>';
                                line += '</div>';
                            line += '</td>';
                        } else {
                            line += '<td class="passed">';
                                line += '<div class="bar bar--' + className + '" data-days="' + numberDays + '" canresize="" ng-click="infoAssign($event);">';
                                    line += '<div class="progress" data-progress="' + progress + '"></div>';
                                line += '</div>';
                            line +='</td>';
                        }
                    } else {
                        if (isLeaf) {
                            line += '<td>';
                                line +='<div class="bar bar--' + className + '" data-days="' + numberDays + '" canresize="" ng-click="infoAssign($event);">';
                                    line += '<div class="progress  leaf" data-progress="' + progress + '">';
                                    line += '</div>';
                                    line +='<span class="progress-text">' + progress + '%</span>';
                                line += '</div>';
                            line += '</td>';
                        } else {
                            line += '<td>';
                                line += '<div class="bar bar--' + className + '" data-days="' + numberDays + '" canresize="" ng-click="infoAssign($event);"></div>';
                                line += '<div class="progress" data-progress="' + progress + '"></div>';
                            line += '</td>';
                        }
                    }

                } else {
                    if (date.getTime() < today.getTime()) {
                        line += '<td class="passed"></td>';
                    } else {
                        line += '<td ng-click="addAssign($event);"></td>';
                    }
                }
            }
            return line;
        }
    } else if (zoomAttr == 'week') {
        console.log('week');
        console.log(week_line);
        var currentDay = firstDateAll.getDay();
        var nbrDayFirstWeek = 6 - currentDay;
        var firstDateFirstWeek = new Date(firstDateAll);
        var lastDateFirstWeek = new Date(firstDateAll);
        lastDateFirstWeek.setDate(lastDateFirstWeek.getDate() + nbrDayFirstWeek);
        week_line += '<td class="week">' + firstDateFirstWeek.getDate() + ' - ' + lastDateFirstWeek.getDate() + '</td>\n';
        for (var date = lastDateFirstWeek; date <= lastDateAll; date.setDate(date.getDate() + 7)) {
            week_line += '<td class="week">' + (date.getDate() + 1) + monthNames[date.getMonth()] + ' - ' + (date.getDate() + 7) + monthNames[date.getMonth()] + '</td>\n';
        }
        console.log(week_line);
        return week_line;
    } else if (zoomAttr = 'month') {

    }
}



angular.module('nomad_project', []).directive('projectsPhases', ['$compile',

    function($compile) {
        return {
            replace: true,
            template: '<table class="projects_table" ng-click="assignement();" data-zoom="default"></table>',
            link: function($scope, elem, attrs) {
                var html = mod = '';
                var start_dates = [];
                var end_dates = [];
                $scope.$watch('dataLoaded', function(newValue, oldValue, scope) {
                    if (newValue == true) {
                        angular.forEach($scope.projects, function(project, index) {
                            start_dates.push(parseDate(project.start_date));
                            end_dates.push(parseDate(project.end_date));
                        });
                        var firstDateAll = new Date(findEarliestDate(start_dates));
                        var lastDateAll = new Date(findLastedDate(end_dates));
                        var today = new Date();
                        var passedDays = getNumberDays(firstDateAll, today);
                        var totalDay = getNumberDays(firstDateAll, lastDateAll);

                        html += '<thead data-dayPassed=' + passedDays + '>';
                        html += '<tr class="head_table">';
                        html += '<td class="headcol">Projects</td>';
                        html += getTableDates(start_dates, end_dates, true, '', firstDateAll, lastDateAll, 'days');
                        html += '</tr>';
                        angular.forEach($scope.projects, function(project, index) {
                            start_dates = [];
                            end_dates = [];

                            start_dates.push(parseDate(project.start_date));
                            end_dates.push(parseDate(project.end_date));
                            $scope.project[project.id] = project.name;
                            $scope.three[project.id] = 4;
                            html += '<tr ' +
                                'class="project" ' +
                                'nc-click="highlight_project([' + project.id + '], [' + project.workers + '])" ' +
                                'ng-dblclick="isolate_project([' + project.id + '],[' + project.workers + '])" ' +
                                'ng-class="{\'op_low\': display_project.indexOf(' + project.id + ') == -1 && display_project.length > 0, \'visually-hidden\': isolate_projects.indexOf(' + project.id + ') == -1 && isolate_projects.length > 0}" data-project-link="' + project.id + '">' +
                                '<td class="headcol">' +
                                '<a href="javascript:void(0);" ' +
                                'class="deployed" ' +
                                'ng-hide="three.' + project.id + ' < 2" ' +
                                'ng-click="deploy($event,' + project.id + ', 1);">&#x25BC;</a>' +
                                '<a href="javascript:void(0);" ' +
                                'class="deployed" ' +
                                'ng-show="three.' + project.id + ' < 2" ' +
                                'ng-click="deploy($event,' + project.id + ', 2);">&#x25b6;</a>' +
                                '<input type="text" ' +
                                'ng-readonly="editProject" ' +
                                'ng-model="project.' + project.id + '" ' +
                                'value="' + project.name + '"/></td>' + getTableDates(start_dates, end_dates, false, 'project', firstDateAll, lastDateAll, project.total_progress) + '</tr>';
                            angular.forEach(project.phases, function(phase, index) {
                                start_dates = [];
                                end_dates = [];
                                start_dates.push(parseDate(phase.start_date));
                                end_dates.push(parseDate(phase.end_date));
                                $scope.project['phase' + phase.id] = phase.name;
                                html += '<tr ' +
                                    'class="phase" ' +
                                    'nc-click="highlight_project([' + project.id + '], [' + project.workers + '])"' +
                                    'ng-dblclick="isolate_project([' + project.id + '],[' + project.workers + '])" ' +
                                    'ng-class="{\'op_low\': display_project.indexOf(' + project.id + ') == -1 && display_project.length > 0, \'visually-hidden\': (isolate_projects.indexOf(' + project.id + ') == -1 && isolate_projects.length > 0) || three.' + project.id + ' < 2}" ' +
                                    'data-project-link="' + project.id + '">' +
                                    '<td ' +
                                    'class="headcol niv-1">' +
                                    '<a href="javascript:void(0);" ' +
                                    'class="deployed" ' +
                                    'ng-hide="three.' + project.id + ' < 3" ng-click="deploy($event,' + project.id + ', 2);">&#x25BC;</a>' +
                                    '<a href="javascript:void(0);" ' +
                                    'class="deployed" ' +
                                    'ng-show="three.' + project.id + ' < 3" ' +
                                    'ng-click="deploy($event,' + project.id + ', 3);">&#x25b6;</a>' +
                                    '<input type="text" ' +
                                    'ng-readonly="editProject" ' +
                                    'ng-model="project.phase' + phase.id + '" ' +
                                    'value="' + phase.name + '" /></td>' + getTableDates(start_dates, end_dates, false, 'phase', firstDateAll, lastDateAll, phase.progress) + '</tr>';
                                angular.forEach(phase.subphases, function(subphase, index) {
                                    start_dates = [];
                                    end_dates = [];
                                    start_dates.push(parseDate(subphase.start_date));
                                    end_dates.push(parseDate(subphase.end_date));
                                    $scope.project['subphase' + subphase.id] = subphase.name;
                                    html += '<tr ' +
                                        'class="subphase" ' +
                                        'nc-click="highlight_project([' + project.id + '], [' + project.workers + '])"' +
                                        'ng-dblclick="isolate_project([' + project.id + '],[' + project.workers + '])" ' +
                                        'ng-class="{\'op_low\': display_project.indexOf(' + project.id + ') == -1 && display_project.length > 0, \'visually-hidden\': (isolate_projects.indexOf(' + project.id + ') == -1 && isolate_projects.length > 0) || three.' + project.id + ' < 3}" ' +
                                        'data-project-link="' + project.id + '">' +
                                        '<td ' +
                                        'class="headcol niv-2">' +
                                        '<a href="javascript:void(0);" ' +
                                        'class="deployed" ' +
                                        'ng-hide="three.' + project.id + ' < 4" ng-click="deploy($event,' + project.id + ', 3);">&#x25BC;</a>' +
                                        '<a href="javascript:void(0);" ' +
                                        'class="deployed" ' +
                                        'ng-show="three.' + project.id + ' < 4" ' +
                                        'ng-click="deploy($event,' + project.id + ', 4);">&#x25b6;</a>' +
                                        '<input ' +
                                        'type="text" ' +
                                        'ng-readonly="editProject" ' +
                                        'ng-model="project.subphase' + subphase.id + '" ' +
                                        'value="' + subphase.name + '" /></td>' + getTableDates(start_dates, end_dates, false, 'sousphase', firstDateAll, lastDateAll, subphase.progress) + '</tr>';
                                    angular.forEach(subphase.tasks, function(task, index) {
                                        start_dates = [];
                                        end_dates = [];
                                        start_dates.push(parseDate(task.start_date));
                                        end_dates.push(parseDate(task.end_date));
                                        $scope.project['task' + task.id] = task.name;
                                        html += '<tr ' +
                                            'class="task" ' +
                                            'nc-click="highlight_project([' + project.id + '], [' + project.workers + '])"' +
                                            'ng-dblclick="isolate_project([' + project.id + '],[' + project.workers + '])" ' +
                                            'ng-class="{\'op_low\': display_project.indexOf(' + project.id + ') == -1 && display_project.length > 0, \'visually-hidden\': (isolate_projects.indexOf(' + project.id + ') == -1 && isolate_projects.length > 0) || three.' + project.id + ' < 4}" ' +
                                            'data-project-link="' + project.id + '">' +
                                            '<td class="headcol niv-3">' +
                                            '<input type="text" ' +
                                            'ng-readonly="editProject"  ' +
                                            'ng-model="project.task' + task.id + '" ' +
                                            'value="' + task.name + '" /></td>' + getTableDates(start_dates, end_dates, false, 'task', firstDateAll, lastDateAll, task.progress, 'days', true) + '</tr>';
                                    });
                                    html += '<tr ' +
                                        'class="task add" ' +
                                        'ng-class="{\'visually-hidden\' : !editProject != true}"><td ' +
                                        'class="headcol niv-3"><input ' +
                                        'type="text" ' +
                                        'ng-readonly="editProject" ' +
                                        'placeholder="+ Add Task" /></td>' + mod + '</tr>';
                                });
                                html += '<tr ' +
                                    'class="subphase add" ' +
                                    'ng-class="{\'visually-hidden\' : !editProject != true}"><td ' +
                                    'class="headcol niv-2"><input ' +
                                    'type="text" ' +
                                    'ng-readonly="editProject" ' +
                                    'placeholder="+ Add Subphase" /></td>' + mod + '</tr>';
                            })
                            html += '<tr ' +
                                'class="phase add" ' +
                                'ng-class="{\'visually-hidden\' : !editProject != true}"><td ' +
                                'class="headcol niv-1"><input ' +
                                'type="text" ' +
                                'ng-readonly="editProject" ' +
                                'placeholder="+ Add Phase" /></td>' + mod + '</tr>';
                        });
                        html += '<tr ' +
                            'class="project add" ' +
                            'ng-class="{\'visually-hidden\' : !editProject != true}"><td ' +
                            'class="headcol"><input type="text" ' +
                            'ng-readonly="editProject" ' +
                            'placeholder="+ Add Project" /></td>' + mod + '</tr>';
                        $scope.backup_projects = angular.copy($scope.project);
                        html += '</thead>';
                        html += '<tbody>';
                        for (var i = 0; i <= totalDay; i++) {
                            if (i < passedDays) {
                                mod += '<td class="passed"></td>';
                            } else {
                                mod += '<td></td>';

                            }

                        }
                        html += '<tr class="head_table"><td class="headcol">Availability</td>' + mod + '</tr>';
                        angular.forEach($scope.workers, function(worker, index) {
                            html += '<tr nc-click="highlight_project([' + worker.projects_id + '],[' + worker.id + '])" ng-dblclick="isolate_project([' + worker.projects_id + '],[' + worker.id + '])"  ng-class="{\'op_low\': display_worker.indexOf(' + worker.id + ') == -1 && display_worker.length > 0, \'visually-hidden\': isolate_worker.indexOf(' + worker.id + ') == -1 && isolate_worker.length > 0}"><td class="headcol">' + worker.first_name + ' ' + worker.last_name + '</td>' + mod + '</tr>';
                        });
                        html += '</tbody>';
                        var el = angular.element(html),
                            compiled = $compile(el);
                        elem.append(el);
                        compiled(scope);
                        $scope.compileDone = true;
                    }
                });
            }
        }
    }
])