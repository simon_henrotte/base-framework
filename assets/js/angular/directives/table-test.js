angular
    .module('table', [])
    .directive('ngTable', ['$compile',
        function($compile) {
            // Runs during compile
            return {
                // name: '',
                priority: 1,
                // terminal: true,
                // scope: {}, // {} = isolate, true = child, false/undefined = no change
                // controller: function($scope, $element, $attrs, $transclude) {},
                // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
                restrict: 'AE', // E = Element, A = Attribute, C = Class, M = Comment
                // templateUrl: '',
                replace: true,
                transclude: true,
                // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
                link: function($scope, elem, attrs, projects) {
                    $scope.$watch('dataLoaded', function(newValue, oldValue, scope) {
                        if (newValue == true) {
                            var html = '<table>';
                            html += '<thead>';
                            angular.forEach($scope.projects, function(project, index) {
                                html += '<tr nc-click="highlight_project([project.id], [project.workers])" ng-dblclick="isolate_project([' + project.id + '],[' + project.workers + '])" ng-class="{\'op_low\': display_project.indexOf(' + project.id + ') == -1 && display_project.length > 0, \'visually-hidden\': isolate_projects.indexOf(' + project.id + ') == -1 && isolate_projects.length > 0}"><td class="headcol">' + project.name + '</td><td class="long">1</td></tr>';
                                angular.forEach(project.phases, function(phase, index) {
                                    console.log('phase');
                                });
                            });
                            html += '</thead>';
                            html += '</table>';
                            elem.replaceWith(html);
                        }
                    });

                }
            };
        }
    ]);