var myApp = angular.module('myApp', []);

myApp.directive('myTable', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                var html = '<table>';
                angular.forEach(scope[attrs.rows], function(row, index) {
                        html += '<tr><td>' + row.name + '</td></tr>';
                        if ('phase' in row) {
                            angular.forEach(row.subrows, function(subrow, index) {
                                html += '<tr><td>' + subrow.name + '</td></tr>';
                                angular.forEach(subrow.sub_phase, function(subphase, index) {
                                    angular.forEach(subphase.tasks, function(task, index) {

                                    });
                                });
                            });
                        };
                        html += '</table>';
                        element.replaceWith(html)
                    }
                }
            });

        function MyCtrl($scope) {
            $scope.rows = [{
                name: 'row1',
                subrows: [{
                    name: 'row1.1'
                }, {
                    name: 'row1.2'
                }]
            }, {
                name: 'row2'
            }];
        }


        Envoyé de mon iPad