var app;

app = angular.module('resizeElem', []);

app.directive("canresize", [
    "$timeout",
    function($timeout) {
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                element.css({
                    "overflow": "hidden"
                });
                return element.resizable({
                    grid: 50,
                    ghost: true,
                    helper: "ui-resizable-helper",
                    handles: "e, w",
                    resize: function(e, ui) {
                        element.width = ui.width;
                        element.attr('data-days', ui.size.width / 50);
                        return
                    }
                });
            }
        };
    }
]);