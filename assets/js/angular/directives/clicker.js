angular.module('clicker', [])
    .directive('ncClick', ['$parse',
        function($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attr) {
                    var fn = $parse(attr['ncClick']);
                    var delay = 200,
                        clicks = 0,
                        timer = null;
                    element.on('click', function(event) {
                        clicks++;
                        if (clicks === 1) {
                            timer = setTimeout(function() {
                                scope.$apply(function() {
                                    fn(scope, {
                                        $event: event
                                    });
                                });
                                clicks = 0;
                            }, delay);
                        } else {
                            clearTimeout(timer);
                            clicks = 0;
                        }
                    });
                }
            };
        }
    ]);