var buildPhasesListDirective;

angular.module('nomad.projects.directives').directive('projectPhases', [

    function() {
        return {
            replace: true,
            scope: {
                phases: '=projectPhases'
            },
            template: '<div class="grid-contain-left showplan-left">\n  <!--<ul project-phases-actions="phases" class="icon-box-contain flush--left"></ul>-->\n  <ul project-phases-name="phases" class="phases-title panel_module"></ul>\n  <ul project-phases-et="phases" class="estimation-time panel_module flush--left"></ul>\n  <ul project-phases-spent="phases" class="spent-time panel_module flush--left"></ul>\n  <ul project-phases-progress="phases" class="prog-time panel_module flush--left"></ul>\n  <ul project-phases-startdate="phases" class="start-time panel_module flush--left"></ul>\n  <ul project-phases-enddate="phases" class="end-time panel_module flush--left"></ul>\n  <ul project-phases-delete="phases" class="icon-box-contain panel_module flush--left"></ul>\n</div>',
            link: function(scope, elem, attrs) {
                return scope.edit = function() {
                    return scope.$parent.edit();
                };
            }
        };
    }
]);

buildPhasesListDirective = function(name, opts) {
    var append, attrName, inputTmpl, ngClass, prepend, repeatTmpl, title;
    if (opts == null) {
        opts = {};
    }
    attrName = opts.attr || name;
    ngClass = "";
    if (attrName !== 'name') {
        ngClass = 'ng-class="{readonly: phase.isTotal || phase.children.length > 0}"';
    }
    if (opts.readonly) {
        ngClass = 'ng-class="{readonly: true}"';
    }
    prepend = append = "";
    if (opts.append != null) {
        prepend = '<div class="fld-group">';
        append = "<span iln-field class='fld_addon fld--iln' " + ngClass + ">" + opts.append + " </span></div>";
    }
    repeatTmpl = "<li project-phase-" + name + "=\"phase\" ng-hide=\"phase._destroy\" ng-repeat=\"phase in phases\"></li>";
    inputTmpl = opts.template || ("" + prepend + "\n<input iln-field ng-model=\"phase." + attrName + "\" name=\"" + attrName + "\" " + ngClass + " " + (opts.inputExtra || '') + "></input>\n" + append);
    title = opts.title || ("{{ 'nomad.projects.spa.show_plan.columns." + name + "' | translate }}");
    return angular.module('nomad.projects.directives').directive("projectPhases" + (name.capitalize()), [

        function() {
            return {
                replace: false,
                scope: {
                    phases: "=projectPhases" + (name.capitalize())
                },
                template: "<li class=\"panel-title\">\n  <span>\n    " + title + "\n  </span>\n</li>\n" + repeatTmpl
            };
        }
    ]).directive("projectPhase" + (name.capitalize()), [
        '$compile',
        function($compile) {
            return {
                replace: false,
                scope: {
                    phase: "=projectPhase" + (name.capitalize())
                },
                template: inputTmpl,
                link: function(scope, elem, attrs) {
                    scope.phases = scope.phase.children;
                    elem.append("<ul>" + repeatTmpl + "</ul>");
                    $compile(elem.find('ul').contents())(scope);
                    if (opts.link != null) {
                        return opts.link(scope, elem, attrs);
                    }
                }
            };
        }
    ]);
};

buildPhasesListDirective("name", {
    inputExtra: 'ng-class="{\'invisible-man\': phase.isTotal}"'
});

buildPhasesListDirective("et", {
    attr: "time_estimated",
    inputExtra: 'type="number" step="0.01" min="0"',
    append: 'd'
});

buildPhasesListDirective("spent", {
    attr: "time_spent",
    readonly: true,
    append: 'd'
});

buildPhasesListDirective("progress", {
    inputExtra: 'type="number" step="1" min="0" max="100"',
    append: '%'
});

buildPhasesListDirective("startdate", {
    attr: "start_date",
    inputExtra: 'datepicker'
});

buildPhasesListDirective("enddate", {
    attr: "end_date",
    inputExtra: 'datepicker'
});

buildPhasesListDirective("actions", {
    title: "&nbsp;",
    template: "",
    link: function(scope, elem, attrs) {}
});

buildPhasesListDirective("delete", {
    title: "&nbsp;",
    template: "<button class=\"btn icon-box-contain-plan\" ng-click=\"removePhase(phase)\" ng-if=\"deletable(phase)\">\n  <span class=\"icon icon-trashcan\"></span>\n</button>\n<span class=\"btn icon-box-contain-plan\" ng-if=\"!deletable(phase)\"></span>\n\n<button class=\"btn icon-box-contain-plan\" ng-click=\"addSubphase(phase)\" title=\"{{ 'nomad.projects.spa.show_plan.actions.add_subphase' | translate }}\" ng-if=\"subphasable(phase)\">\n  <span class=\"icon icon-plus\"></span>\n</button>",
    link: function(scope, elem, attrs) {
        scope.deletable = function(phase) {
            return !(phase.isTotal || phase.time_spent > 0);
        };
        scope.subphasable = function(phase) {
            return phase.isParent || (phase.time_spent == null) || phase.time_spent === 0;
        };
        scope.edit = scope.$parent.$parent.edit || scope.$parent.$parent.$parent.edit;
        scope.addSubphase = function(phase) {
            phase.children.push({
                parent_id: phase.id,
                progress: 0,
                children: []
            });
            return setTimeout(function() {
                scope.edit();
                return elem.find("li:last input").focus();
            }, 0);
        };
        return scope.removePhase = function(phase) {
            return phase._destroy = true;
        };
    }
});